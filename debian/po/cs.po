# Czech PO debconf template translation of google-android-m2repository-installer.
# Copyright (C) 2016 Michal Simunek <michal.simunek@gmail.com>
# This file is distributed under the same license as the google-android-m2repository-installer package.
# Michal Simunek <michal.simunek@gmail.com>, 2016.
#
msgid ""
msgstr ""
"Project-Id-Version: google-android-m2repository-installer 35\n"
"Report-Msgid-Bugs-To: google-android-installers@packages.debian.org\n"
"POT-Creation-Date: 2021-10-17 12:58+0200\n"
"PO-Revision-Date: 2016-08-12 09:02+0200\n"
"Last-Translator: Michal Simunek <michal.simunek@gmail.com>\n"
"Language-Team: Czech <debian-l10n-czech@lists.debian.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: select
#. Description
#: ../templates.in:1001
msgid "Mirror to download packages ?"
msgstr "Ze kterého zrcadla se mají stahovat balíčky?"

#. Type: select
#. Description
#: ../templates.in:1001
msgid ""
"Please select your preferred mirror to download Google's Android packages "
"from."
msgstr ""
"Vyberte si prosím upřednostňované zrcadlo, ze kterého se mají stahovat "
"balíčky pro Google Android."
